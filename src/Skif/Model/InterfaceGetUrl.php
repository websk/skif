<?php

namespace Skif\Model;

interface InterfaceGetUrl {
    public function getUrl();
}