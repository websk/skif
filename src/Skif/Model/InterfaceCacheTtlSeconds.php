<?php

namespace Skif\Model;

/**
 * Interface InterfaceCacheTtlSeconds
 * @package Skif\Model
 */
interface InterfaceCacheTtlSeconds {
    public function getCacheTtlSeconds();
}
