<?php

namespace Skif\Model;

/**
 * Interface InterfaceLogger
 * @package Skif\Model
 * Если класс реализует этот интерфейс, то он его изменения логгируются
 */
interface InterfaceLogger {
}