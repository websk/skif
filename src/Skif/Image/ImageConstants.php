<?php

namespace Skif\Image;

class ImageConstants {
	const IMG_ROOT_FOLDER = 'files' . DIRECTORY_SEPARATOR . 'images';
	const IMG_PRESETS_FOLDER = 'cache';
	const DEFAULT_UPLOAD_PRESET = 'upload';
}
